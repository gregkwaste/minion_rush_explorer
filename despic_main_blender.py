bl_info = {
    "name": "Pig Model Importer",
    "description": "Designed to import Minion Rush 3d models",
    "author": "gregkwaste",
    "version": (0, 5, 'beta'),
    "blender": (2, 71, 0),
    "location": "View3D > Scene",
    "warning": "",  # used for warning icon and text in addons panel
    "wiki_url": "",
    "tracker_url": "",
    "category": "Import-Export"}


import struct
import bpy
import bmesh
import os
from bpy_extras.io_utils import ImportHelper

version_text = 'v0.5 Coded by gregkwaste'


class pigimporter(bpy.types.Operator, ImportHelper):
    bl_label = 'PIG Importer Panel'
    bl_idname = 'pig_importer.importer'
    # bl_space_type = 'PROPERTIES'
    # bl_region_type = 'WINDOW'
    # bl_context = 'scene'

    # ImportHelper mixin class uses this
    filename_ext = ".pig"

    filter_glob = bpy.props.StringProperty(
        default="*.pig",
        options={'HIDDEN'},
    )

    def execute(self, context):
        return model_import(context.scene, self.filepath)


def skip_forward(f, val):
    valdict = {}
    valdict[4] = '<I'
    valdict[2] = '<H'
    count = 0
    while True:
        step = struct.unpack(valdict[val], f.read(val))[0]
        count += val
        if (count > 10):
            return -1

        if step == 0:
            continue
        elif count <= 6:
            f.seek(-val, 1)
            return 1
        else:
            f.seek(-val, 1)
            return 0

    return 0


def advance_forward(f):
    step = struct.unpack('<H', f.read(2))[0]
    f.read(step)


def read_str(f):
    str = ''
    str_len = struct.unpack('<H', f.read(2))[0]
    for i in range(str_len):
        str += chr(struct.unpack('<B', f.read(1))[0])
    return str


def read_vertices_1(f):
    vert_tup = struct.unpack('<3f', f.read(12))
    return((vert_tup[0], vert_tup[1], vert_tup[2]))


def read_vertices_2(f):
    vert_tup = struct.unpack('<3h', f.read(6))
    # print(vert_tup)
    return((vert_tup[0] / 32768, vert_tup[1] / 32768, vert_tup[2] / 65535))


def read_uvs_1(f):
    return(struct.unpack('<2f', f.read(8)))


def read_uvs_2(f):
    vert_tup = struct.unpack('<2h', f.read(4))
    return (vert_tup[0] / 32768, vert_tup[1] / 32768)


def read_triangle_2(f):
    temp = (struct.unpack('<3H', f.read(6)))
    if (temp[0] != temp[2] and temp[1] != temp[0] and temp[1] != temp[2]):
        return (temp[0], temp[2], temp[1])


def read_cols_0(f):
    val = struct.unpack('<4B', f.read(4))
    return((val[0] / 255, val[1] / 255, val[2] / 255))


# Create Mesh and Object Function
def createmesh(verts, faces, uvs, name, count, id, subname, colors, normal_flag, normals):
    scn = bpy.context.scene
    mesh = bpy.data.meshes.new("mesh" + str(count))
    mesh.from_pydata(verts, [], faces)

    for i in range(len(uvs)):
        uvtex = mesh.uv_textures.new(name='map' + str(i))
    for i in range(len(colors)):
        coltex = mesh.vertex_colors.new(name='col' + str(i))

    bm = bmesh.new()
    bm.from_mesh(mesh)

    # Create UV MAPS
    for i in range(len(uvs)):
        uvlayer = bm.loops.layers.uv['map' + str(i)]
        for f in bm.faces:
            for l in f.loops:
                l[uvlayer].uv.x = uvs[i][l.vert.index][0]
                l[uvlayer].uv.y = 1 - uvs[i][l.vert.index][1]

    # Create VERTEX COLOR MAPS

    for i in range(len(colors)):
        collayer = bm.loops.layers.color['col' + str(i)]
        for f in bm.faces:
            for l in f.loops:
                l[collayer].r = colors[i][l.vert.index][0]
                l[collayer].g = colors[i][l.vert.index][1]
                l[collayer].b = colors[i][l.vert.index][2]

    # Pass Normals
    if normal_flag == True:
        for i in range(len(normals)):
            bm.verts[i].normal = normals[i]

    # Finish up, write the bmesh back to the mesh
    bm.to_mesh(mesh)
    bm.free()  # free and prevent further access

    if name.split(sep='_')[0] == 'head':
        object = bpy.data.objects.new(
            name + '_' + str(id) + '_' + str(count) + '_' + subname.split(sep='_')[0], mesh)
    else:
        object = bpy.data.objects.new(
            name + '_' + str(id) + '_' + str(count), mesh)

    object.location = (0, 0, 0)

    # Transformation attributes inherited from bounding boxes
    # if not name=='stadium':
    # object.scale=Vector((0.1,0.1,0.1))
    # object.rotation_euler=Euler((1.570796251296997, -0.0, 0.0), 'XYZ')
    bpy.context.scene.objects.link(object)

    return object.name


def read_sect(f):
    f.read(4)  # READ 0x64000000
    name = read_str(f)
    f.read(3)  # READ 0x010000
    val_1 = struct.unpack('<I', f.read(4))[0]
    val_2 = struct.unpack('<I', f.read(4))[0]
    f.read(32)  # READ 16 byte float section (propably bound box)
    val_3 = struct.unpack('<I', f.read(4))[0]
    f.read(2)
    return [name, val_1, val_2, val_3]


def model_import(scn, path):
    sect_db = []
    fileName = os.path.basename(path)
    dirName = os.path.dirname(path)
    modelName = fileName.split('.')[0]
    f = open(path, 'rb')
    f.read(4)  # READ 0x64000000
    items = struct.unpack('<H', f.read(2))[0]
    print('Bones In section: ', items)

    for i in range(items):
        sect_db.append(read_sect(f))

    f.read(3)  # Read 0x010100
    f.read(11)  # Read Empty Section
    # 32 byte Section
    f.read(30)  # Read Probably Bound Box
    subitems_count = struct.unpack('<H', f.read(2))[0]

    for part_id in range(subitems_count):
        # Mesh Description
        start = f.tell()
        f.read(4)  # Read 0x64000000
        object_type = struct.unpack('<H', f.read(2))[0]
        f.read(2)  # Read remnant
        sub_type = struct.unpack('<I', f.read(4))[0]
        if object_type == 0x29:
            f.read(24)
        f.read(12)  # Read 3 floats
        vert_num = struct.unpack('<H', f.read(2))[0]
        indices_num = struct.unpack('<I', f.read(4))[0]
        print('*******************************************')
        print('Model Start: ', start)
        print('Subpart: ', part_id + 1, 'of',
              subitems_count, 'Object Type:', object_type, 'SubType: ', sub_type)
        print('Vertices Number: ', vert_num, 'Indices Number', indices_num)

        # Material Section
        mat_name = read_str(f)
        print('Material Name: ', mat_name)
        mat_id = struct.unpack('<H', f.read(2))[0]
        if mat_name in bpy.data.materials:
            mat = bpy.data.materials[mat_name]
        else:
            mat = bpy.data.materials.new(modelName + '_' + mat_name)
            mat.use_shadeless = True
            mat.use_transparency = True
            mat.alpha = 0.0

        print('Textures')
        # Textures
        textures = []
        tex_count = 3  # (+1)
        i = 0
        while (i < tex_count):
            print(f.tell())
            texname = read_str(f)
            if not texname:
                # Empty Slot
                cond = skip_forward(f, 2)
                if cond == -1:  # Count Exceeded
                    break
                elif cond == 1:
                    continue
                elif cond == 0:
                    i += 1
                    continue
            f.read(2)
            textures.append(texname)
            i += 1

        print('End of Textures :', f.tell())

        f.read(1)
        # Aligning
        # step = (f.tell() - start) % 4
        # print('Aligning with : ', step)
        # f.read(step)
        skip_forward(f, 2)  # Parsing White Space
        skip_forward(f, 2)  # Parsing Remaining Space

        print('End of Alignment :', f.tell())
        # Trying to create textures in Blender
        for texname in textures:
            texname = str.lower(texname)
            if texname not in bpy.data.textures:
                try:
                    img = bpy.data.images.load(os.path.join(dirName, 'textures', texname))
                except:
                    print('Missing Texture', texname)
                    continue
                tex = bpy.data.textures.new(texname, 'IMAGE')
                tex.image = img

            else:
                print('Texture Exists')
                tex = bpy.data.textures[texname]
            slot = mat.texture_slots.add()
            slot.texture = tex
            slot.texture_coords = 'UV'
            slot.use_map_alpha = True
            slot.alpha_factor = 1.0

        # f.read(1)
        # print(f.tell())
        # First Data Section Size
        data_size = struct.unpack('<I', f.read(4))[0]
        advance_forward(f)

        # Parsing Color Section
        if (sub_type & 0x40):
            colors = []
            temp = []
            for i in range(vert_num):
                temp.append(read_cols_0(f))
            colors.append(temp)
            advance_forward(f)
            print('End of colors: ', f.tell())

        # Parsing Uv Section
        if (sub_type & 0x80):
            uvs = []
            temp = []
            for i in range(vert_num):
                temp.append(read_uvs_1(f))
            uvs.append(temp)
            advance_forward(f)
            print('End of uvs: ', f.tell())

        # Skipping check with 0x40,0x20,0x10

        # Triangles
        faces = []
        for i in range(indices_num // 3):
            faces.append(read_triangle_2(f))

        print('End of trianges: ', f.tell())

        elem_size = struct.unpack('<H', f.read(2))[0]
        if elem_size <= 3:
            bone_count = struct.unpack('<H', f.read(2))[0]
        elif elem_size == 4:
            bone_count = struct.unpack('<I', f.read(elem_size))[0]

        wtf_size = struct.unpack('<I', f.read(4))[0]

        # Skipping Bones(????)
        f.read(bone_count * (28 + 64))

        print('Start of Vertex Data Section', f.tell())
        # Second Vertex Based Data Section
        verts = []
        normals = []
        binormals = []
        for i in range(vert_num):
            f.read(4)  # Skip one number
            f.read(16)  # Skip some vertex Data, probably always there
            if (sub_type & 1):
                verts.append(read_vertices_1(f))  # .xyz
                f.read(4)  # .w
            if (sub_type & 2):
                normals.append(read_vertices_1(f))
                f.read(4)
            if (sub_type & 4):
                binormals.append(read_vertices_1(f))
                f.read(4)

        obname = createmesh(
            verts, faces, uvs, modelName, part_id, 0, '', [], False, [])
        bpy.data.objects[obname].data.materials.append(mat)
    return {'FINISHED'}


# -----------------Blender PROPERTIES--------------

# Only needed if you want to add into a dynamic menu
def menu_pigimport(self, context):
    self.layout.operator(pigimporter.bl_idname, text="Gameloft PIG (.pig)")


def register():
    bpy.utils.register_class(pigimporter)
    bpy.types.INFO_MT_file_import.append(menu_pigimport)


def unregister():
    bpy.utils.unregister_class(pigimporter)
    bpy.types.INFO_MT_file_import.remove(menu_pigimport)


if __name__ == "__main__":
    register()
