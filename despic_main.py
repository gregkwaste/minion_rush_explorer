import struct

def skip_forward(f):
    step=struct.unpack('<H',f.read(2))[0]
    if step==0:
        skip_forward(f)
    else:
        f.seek(-2,1)


def advance_forward(f):
    step=struct.unpack('<H',f.read(2))[0]
    f.read(step)

def read_str(f):
    str=''
    str_len=struct.unpack('<H',f.read(2))[0]
    for i in range(str_len):
        str+=chr(struct.unpack('<B',f.read(1))[0])
    return str

def read_vertices_1(f):
    vert_tup=struct.unpack('<3f',f.read(12))
    return((vert_tup[0],-vert_tup[1],vert_tup[2]))

def read_vertices_2(f):
	vert_tup=struct.unpack('<3h',f.read(6))
	#print(vert_tup)
	return((vert_tup[0]/65535,vert_tup[1]/65535,vert_tup[2]/65535))


def read_uvs_1(f):
    return(struct.unpack('<2f',f.read(8)))

def read_uvs_2(f):
	vert_tup=struct.unpack('<2h',f.read(4))
	return (vert_tup[0]/65535,vert_tup[1]/65535)


def read_triangle_2(f):
    return(struct.unpack('<3H',f.read(6)))

def read_cols_0(f):
    val=struct.unpack('<4B',f.read(4))
    return((val[0]/255,val[1]/255,val[2]/255))


def read_sect(f):
    f.read(4)#READ 0x64000000
    name=read_str(f)
    f.read(3)#READ 0x010000
    val_1=struct.unpack('<I',f.read(4))[0]
    val_2=struct.unpack('<I',f.read(4))[0]
    f.read(32) #READ 16 byte float section (propably bound box)
    val_3=struct.unpack('<I',f.read(4))[0]
    f.read(2)
    return [name,val_1,val_2,val_3]


sect_db=[]
#PC FILES
#f=open('C:\\Python33\\models.bin','rb')
#IPHONE FILES
iphone_version=True
iphone_dict={}
iphone_dict[41]={}
iphone_dict[41][135]='vccnccib'
iphone_dict[41][129]='vnccib'
iphone_dict[105]={}
iphone_dict[105][129]='vcuuib'
iphone_dict[105][135]='vcuuccib'
iphone_dict[105][195]='vccuccib'



f=open('C:\\Python33\\dlc_v2_costumes_ios_costumes_26.jpk','rb')


f.seek(5593296)
print('Model Offset: ',f.tell())
f.read(4)##READ 0x64000000
items=struct.unpack('<H',f.read(2))[0]
print('Items In section: ',items)

for i in range(items):
    sect_db.append(read_sect(f))

#Read 0x010100
f.read(3)

f.read(11) #Read Empty Section
#32 byte Section
f.read(30) #Read Unknown Part
subitems_count=struct.unpack('<H',f.read(2))[0]



for part_id in range(subitems_count):
    print(f.tell())
    #Mesh Description
    f.read(4) #Read 0x64000000
    object_type=struct.unpack('<H',f.read(2))[0]
    f.read(2) #Read remnant
    sub_type=struct.unpack('<I',f.read(4))[0]
    f.read(36)
    vert_num=struct.unpack('<H',f.read(2))[0]
    indices_num=struct.unpack('<H',f.read(2))[0]
    print('*******************************************')
    print('Subpart: ',part_id+1,'of',subitems_count, 'Object Type: ',object_type,'Subtype: ',sub_type)
    print('Vertices Number: ',vert_num,'Indices Number',indices_num)

    f.read(2) #0x0000
    #Material Section
    mat_name=read_str(f)
    mat_id=struct.unpack('<H',f.read(2))[0]

    #Textures
    textures=[]
    exp=0
    while True:
        textures.append(read_str(f))
        print(textures[-1])
        f.read(2)
        skip_forward(f)
        test=struct.unpack('<H',f.read(2))[0]
        if test<30:
            f.seek(-2,1)
            continue
        else:
            f.seek(-2,1)
            break

    print(f.tell())
    f.read(1)
    data_size=struct.unpack('<I',f.read(4))[0]

    if iphone_version:
        uvs=[]
        cols=[]
        verts=[]
        normals=[]
        faces=[]
        #Model Data
        advance_forward(f)
        for l in iphone_dict[object_type][sub_type]:
            if l=='v':

                #Verts
                verts=[]
                for i in range(vert_num):
                    verts.append(read_vertices_2(f))
                    f.read(2)
                advance_forward(f)
            elif l=='n':
                #Normals
                for i in range(vert_num):
                    normals.append(read_vertices_2(f))
                    f.read(2)
                advance_forward(f)
            elif l=='u':
                temp=[]
                for i in range(vert_num):
                    temp.append(read_uvs_2(f))
                uvs.append(temp)
                advance_forward(f)
            elif l=='c':
                #color map
                temp=[]
                for i in range(vert_num):
                    temp.append(read_cols_0(f))
                cols.append(temp)
                advance_forward(f)
            elif l=='i':
                for i in range(indices_num//3):
                    faces.append(read_triangle_2(f))
            elif l=='b':
                #BONE SECTION
                elem_size=struct.unpack('<H',f.read(2))[0]
                if elem_size==2 or elem_size==3:
                    bone_count=struct.unpack('<H',f.read(2))[0]
                elif elem_size==4:
                    bone_count=struct.unpack('<I',f.read(elem_size))[0]

                wtf_size=struct.unpack('<I',f.read(4))[0]

                #Skipping Bones(????)
                f.read(wtf_size)
    else:

        if object_type==195:
            #Model Data:
            advance_forward(f)
            #Verts
            verts=[]
            for i in range(vert_num):
                verts.append(read_vertices_1(f))
            advance_forward(f)
            #Normals
            normals=[]
            for i in range(vert_num):
                normals.append(read_vertices_1(f))
            advance_forward(f)
            #Colors
            colors=[]
            for i in range(vert_num):
                colors.append(read_cols_0(f))
            advance_forward(f)
            #Uvs
            uvs=[]
            for i in range(vert_num):
                uvs.append(read_uvs_1(f))
            advance_forward(f)
            #Triangles
            faces=[]
            for i in range(indices_num//6):
                faces.append(read_triangle_2(f))

        elif object_type in [135,129]:

            #Model Data:
            advance_forward(f)
            print('Reading starts at:',f.tell())
            #Uvs
            uvs=[]
            for i in range(vert_num):
                uvs.append(read_uvs_1(f))
            advance_forward(f)
            #Triangles
            faces=[]
            for i in range(indices_num//3):
                faces.append(read_triangle_2(f))

            #print(f.tell())
            elem_size=struct.unpack('<H',f.read(2))[0]
            if elem_size==2 or elem_size==3:
                bone_count=struct.unpack('<H',f.read(2))[0]
            elif elem_size==4:
                bone_count=struct.unpack('<I',f.read(elem_size))[0]

            wtf_size=struct.unpack('<I',f.read(4))[0]

            #Skipping Bones(????)
            f.read(bone_count*(28+64))

            #Vertices
            verts=[]
            if object_type==129:
                for i in range(vert_num):
                    f.read(4)
                    f.read(16)
                    verts.append(read_vertices_1(f))
                    f.read(4)

            else:
                for i in range(vert_num):
                    f.read(4)
                    verts.append(read_vertices_1(f))
                    f.read(4)

                    f.read(16)
                    f.read(16)
                    f.read(16)


        elif object_type in [199]:
            #Model Data
            advance_forward(f)
            #Colors
            colors=[]
            for i in range(vert_num):
                colors.append(read_cols_0(f))
            advance_forward(f)
            #Uvs
            uvs=[]
            for i in range(vert_num):
                uvs.append(read_uvs_1(f))
            advance_forward(f)
            #Triangles
            faces=[]
            for i in range(indices_num//3):
                faces.append(read_triangle_2(f))



            #print(f.tell())
            elem_size=struct.unpack('<H',f.read(2))[0]
            if elem_size==2 or elem_size==3:
                bone_count=struct.unpack('<H',f.read(2))[0]
            elif elem_size==4:
                bone_count=struct.unpack('<I',f.read(elem_size))[0]

            wtf_size=struct.unpack('<I',f.read(4))[0]

            #Skipping Bones(????)
            f.read(bone_count*(28+64))

            #Vertices
            verts=[]

            for i in range(vert_num):
                f.read(4)
                f.read(16)

                verts.append(read_vertices_1(f))
                f.read(4)
                f.read(16)
                f.read(16)
