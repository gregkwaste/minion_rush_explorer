import struct


def read_str(f):
    str = ''
    str_len = struct.unpack('<I', f.read(4))[0]
    for i in range(str_len):
        str += chr(struct.unpack('<B', f.read(1))[0])
    return str


def read_str_n(f, n):
    str = ''
    for i in range(n):
        str += chr(struct.unpack('<B', f.read(1))[0])
    return str


def load_filedb(f):
    # databases
    filedb = []

    # f=open('C:\\Python33\\textures_w8.bin.hdr','rb')

    # f = open('C:\\Python33\\models.bin.hdr', 'rb')

    file_count = struct.unpack('<I', f.read(4))[0]
    path = read_str(f)

    f.read(18)

    for i in range(file_count - 1):
        entry = []
        entry.append(read_str(f))
        entry.append(struct.unpack('<I', f.read(4))[0])
        f.read(4)
        entry.append(struct.unpack('<I', f.read(4))[0])
        entry.append(struct.unpack('<I', f.read(4))[0])
        f.read(2)
        filedb.append(entry)
        # print('Name: ',entry[0],'Offset: ',entry[1],'Sizes: ',entry[2],entry[3])

    return filedb


def load_filedb_jpk(f):
    file_length = len(f.read())
    f.seek(0)
    filedb = []
    while f.tell() <= file_length:

        try:
            if struct.unpack('<I', f.read(4))[0] == 67324752:
                # main_offset = f.tell() - 4
                f.read(18)  # Skip useless info
                main_size = struct.unpack('<I', f.read(4))[0]
                # f.read(4) #Skip Second Size
                file_name = read_str(f)
                data_offset = f.tell()
                f.read(main_size)
                entry = (file_name, data_offset, main_size)
                filedb.append(entry)
            else:
                # print(f.tell())
                f.read(4)
        except(struct.error):
            # print('End Of File Section')
            break

    return filedb


# offsets.sort()

# print('First Offset: ', offsets[0])
