import bpy,bmesh,struct,io,os,imp,math,sys
from mathutils import Matrix,Vector
halfpath='fifa_tools\\scripts\\source\\half.py'
half=imp.load_source('half',halfpath)
comp=half.Float16Compressor()



#Create Mesh and Object Function
def createmesh(verts,faces,uvs,name,count,id,subname,colors,normal_flag,normals):
    scn=bpy.context.scene
    mesh=bpy.data.meshes.new("mesh"+str(count))
    mesh.from_pydata(verts,[],faces)
    

    
    
    for i in range(len(uvs)):
        uvtex=mesh.uv_textures.new(name='map'+str(i))
    for i in range(len(colors)):
        coltex=mesh.vertex_colors.new(name='col'+str(i))
    
        
        
    bm=bmesh.new()
    bm.from_mesh(mesh)
    
    #Create UV MAPS
    for i in range(len(uvs)):
        uvlayer=bm.loops.layers.uv['map'+str(i)]
        for f in bm.faces:
            for l in f.loops:
                l[uvlayer].uv.x=uvs[i][l.vert.index][0]
                l[uvlayer].uv.y=1-uvs[i][l.vert.index][1]
    

    #Create VERTEX COLOR MAPS
    
    for i in range(len(colors)):
        collayer=bm.loops.layers.color['col'+str(i)]
        for f in bm.faces:
                for l in f.loops:
                        l[collayer].r=colors[i][l.vert.index][0]
                        l[collayer].g=colors[i][l.vert.index][1]
                        l[collayer].b=colors[i][l.vert.index][2]
            
    
    
    #Pass Normals
    if normal_flag==True:
        for i in range(len(normals)):
            bm.verts[i].normal=normals[i]
     

    # Finish up, write the bmesh back to the mesh
    bm.to_mesh(mesh)
    bm.free()  # free and prevent further access
    
    
    if name.split(sep='_')[0]=='head':
        object=bpy.data.objects.new(name+'_'+str(id)+'_'+str(count)+'_'+subname.split(sep='_')[0],mesh)
    else:
        object=bpy.data.objects.new(name+'_'+str(id)+'_'+str(count),mesh)
    
    object.location=(0,0,0)
    
    #Transformation attributes inherited from bounding boxes
    #if not name=='stadium': 
        #object.scale=Vector((0.1,0.1,0.1))
        #object.rotation_euler=Euler((1.570796251296997, -0.0, 0.0), 'XYZ')
    bpy.context.scene.objects.link(object)
    
    return object.name

def read_vertices_1(f):
    vert_tup=struct.unpack('<3f',f.read(12))
    return((vert_tup[0]/100,-vert_tup[2]/100,vert_tup[1]/100)) 

def read_vertices_big(f):
    vert_tup=struct.unpack('>3f',f.read(12))
    return((vert_tup[0],-vert_tup[2],vert_tup[1]))  

    
def read_vertices_2(f,endian):
    vert_tup=struct.unpack(endian+'3h',f.read(6))
    print(vert_tup)
    return((vert_tup[0]/65535,vert_tup[1]/65535,vert_tup[2]/65535))     

def read_verts_as_uvs_1(f):
    vert_tup=struct.unpack('<2f',f.read(8))
    return (vert_tup[0],vert_tup[1],0)

def read_verts_as_uvs_2(f):
    vert_tup=struct.unpack('<2h',f.read(4))
    print(vert_tup)
    return (vert_tup[0]/65535,vert_tup[1]/65535,0)

def read_vertices_2n(f):
    vert_tup = struct.unpack('<3h', f.read(6))
    # print(vert_tup)
    return((vert_tup[0] / 65535, vert_tup[1] / 65535, vert_tup[2] / 65535))

    
    
def read_vertices_0l(f):
    vx=comp.decompress(struct.unpack('<H',f.read(2))[0])
    vy=comp.decompress(struct.unpack('<H',f.read(2))[0])
    vz=comp.decompress(struct.unpack('<H',f.read(2))[0])
    

    hx=struct.unpack('f',struct.pack('I',vx))[0]
    hy=struct.unpack('f',struct.pack('I',vy))[0]
    hz=struct.unpack('f',struct.pack('I',vz))[0]
    print(hx,hy,hz)
    return((hx,hy,hz))  

def read_vertices_0b(f):
    vx=comp.decompress(struct.unpack('>H',f.read(2))[0])
    vy=comp.decompress(struct.unpack('>H',f.read(2))[0])
    vz=comp.decompress(struct.unpack('>H',f.read(2))[0])
    

    hx=struct.unpack('f',struct.pack('I',vx))[0]
    hy=struct.unpack('f',struct.pack('I',vy))[0]
    hz=struct.unpack('f',struct.pack('I',vz))[0]
    print(hx,hy,hz)
    return((hx,hy,hz))


def vec_roll_to_mat3(vec, roll):
    target = Vector((0,1,0))
    nor = vec.normalized()
    axis = target.cross(nor)
    if axis.dot(axis) > 0.000001:
        axis.normalize()
        theta = target.angle(nor)
        bMatrix = Matrix.Rotation(theta, 3, axis)
    else:
        updown = 1 if target.dot(nor) > 0 else -1
        bMatrix = Matrix.Scale(updown, 3)
    rMatrix = Matrix.Rotation(roll, 3, nor)
    mat = rMatrix * bMatrix
    return mat

def mat3_to_vec_roll(mat):
    vec = mat.col[1]
    vecmat = vec_roll_to_mat3(mat.col[1], 0)
    vecmatinv = vecmat.inverted()
    rollmat = vecmatinv * mat
    roll = math.atan2(rollmat[0][2], rollmat[2][2])
    return vec, roll

    
#f=open('G:\\Blender\\blender-2.71-windows64\\fifa_tools\\simjersey_0_0_0_0_0.rx3.decompressed','rb')
#f=open('G:\\data\sceneassets\\simplecloth\\simhair_182836_0_0.rx3','rb')
#f=open('C:\\Users\\gregkwaste\\Documents\\stadium_113.rad','rb')
#f=open('C:\\Users\\gkass_000\\Dropbox\\temp\\stadium_113.rad','rb')
f = open(
    "L:\\Users\\gregkwaste\\Documents\\Projects\\Minion Rush Explorer\\mc_001.pig", "rb")
#f=open("L:\\Downloads\\mc_001.pig",'rb')

verts=[]
verts1=[]
face_ids=[]
print('Start Reading')
b=2
#VERTICES
vert_count=0x1D
f.seek(0x186E)
#f.seek(10,1)
bone_offset=0x2D290

verts=[]
for i in range(vert_count):
    #v1=struct.unpack('<h',f.read(2))[0]/65536
    #v2=struct.unpack('<h',f.read(2))[0]/65536
    #v3=struct.unpack('<h',f.read(2))[0]/65536
    #f.read(8)
    #verts.append((v1,v2,v3))
    verts.append(read_vertices_2n(f))
    f.read(2)
    
createmesh(verts,[],[],'rad__'+str(2),0,0,'',[],False,[])
    
print(hex(f.tell()))    
raise OSError()


f.read(16)        

for i in range(900):
    v1=struct.unpack('<f',f.read(4))[0]/100
    v2=struct.unpack('<f',f.read(4))[0]/100
    v3=struct.unpack('<f',f.read(4))[0]/100
    f.read(0xC)
    #f.read(0x70)
    if not (v1 and v2 and v3):
        continue
    else:
        verts1.append((v1,-v3,v2))


print(hex(f.tell()))
        
        
    
#FACES
#f.seek(911871)

#for i in range(1300):
#   face_ids.append(struct.unpack('>3H',f.read(6)))
    
#UVS
uvs_0=[]
uvs_1=[]
#f. seek(54597)
#for i in range(vert_count):
#   uvs_0.append(struct.unpack('>2f',f.read(8)))
#f.seek(71479)
#for i in range(vert_count):
#   uvs_1.append(struct.unpack('>2f',f.read(8)))

    
uvs=[]

#uvs.append(uvs_0)
#uvs.append(uvs_1)

    
    
#createmesh(verts,[],[],'rad_1',0,0,'',[],False,[])
#createmesh(verts1,[],[],'rad_2',0,0,'',[],False,[])


#f.seek(0x7480)

#log=open('log.txt','w')
#for i in range(4334):
#    log.write(str(struct.unpack('<f',f.read(4))[0])+' '+str(struct.unpack('<2H',f.read(4)))+'\n')
#log.close()    
    
#f.seek(bone_offset)
#print(f.read(16))
#f.seek(0x10,1)

#bc=392

#amt = bpy.data.armatures['test_arm']
#ob = bpy.data.objects.new('test_arm_object', amt)

#ob=bpy.data.objects['test_arm']

#bpy.context.scene.objects.active = ob
#bpy.ops.object.mode_set(mode='EDIT')
#
#for i in range(5):
#    bone = amt.edit_bones.new('mynewnewbone'+'_'+str(i))
#    mat=Matrix()
#    mat=mat.to_4x4()
#    for i in range(4):
#        for j in range(4):
#            mat[j][i]=round(struct.unpack('<f',f.read(4))[0],8)
#    
#    print(mat)
#    #bone.matrix=mat
#    pos = mat.to_translation()
#    axis, roll = mat3_to_vec_roll(mat.to_3x3())
#
#    bone.head = pos
#    bone.tail = pos + axis
#    bone.roll = roll
#    
#
#bpy.ops.object.mode_set(mode='OBJECT')  
#    
f.close()   



    
    
    













