# -*- coding: utf-8 -*-
"""
Created on Sun Feb 09 17:41:49 2014

@author: gkass_000
"""

# Minion Rush Iphone dlc Files Identifier
import struct

# Functions


def read_str(f):
    str = ''
    str_len = struct.unpack('<H', f.read(2))[0]
    gap = struct.unpack('<H', f.read(2))[0]
    # print(str_len,gap)
    for i in range(str_len):
        str += chr(struct.unpack('<B', f.read(1))[0])
    # print(f.tell())
    f.read(gap)
    return str


files = []

f = open('/media/TI31107400A/Python33/dlc_v2_costumes_ios_costumes_26.jpk', 'rb')


file_length = len(f.read())
f.seek(0)

while f.tell() <= file_length:

    try:
        if struct.unpack('<I', f.read(4))[0] == 67324752:
            main_offset = f.tell() - 4
            f.read(18)  # Skip useless info
            main_size = struct.unpack('<I', f.read(4))[0]
            # f.read(4) #Skip Second Size
            file_name = read_str(f)
            data_offset = f.tell()
            f.read(main_size)
            files.append((file_name, data_offset, main_size))
        else:
            # print(f.tell())
            f.read(4)
    except(struct.error):
        print('End Of File Section')
        break
