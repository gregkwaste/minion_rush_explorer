from PySide.QtCore import *
from PySide.QtGui import *


import operator
import sys


class TreeItem(object):

    def __init__(self, data, parent=None):
        self.parentItem = parent
        self.itemData = data
        self.childItems = []

    def appendChild(self, item):
        self.childItems.append(item)

    def child(self, row):
        return self.childItems[row]

    def childCount(self):
        return len(self.childItems)

    def columnCount(self):
        return len(self.itemData)

    def data(self, column):
        try:
            return self.itemData[column]
        except IndexError:
            return None

    def parent(self):
        return self.parentItem

    def row(self):
        if self.parentItem:
            return self.parentItem.childItems.index(self)

        return 0


class TreeModel(QAbstractItemModel):
    progressTrigger = Signal(int)

    def __init__(self, columns, parent=None):
        super(TreeModel, self).__init__(parent)

        self.rootItem = TreeItem(columns)
        # self.setupModelData(data, self.rootItem)

    def columnCount(self, parent):
        if parent.isValid():
            return parent.internalPointer().columnCount()
        else:
            return self.rootItem.columnCount()

    def data(self, index, role):
        if not index.isValid():
            return None

        if role != Qt.DisplayRole:
            return None

        item = index.internalPointer()

        return item.data(index.column())

    def flags(self, index):
        if not index.isValid():
            return Qt.NoItemFlags

        return Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def headerData(self, section, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.rootItem.data(section)

        return None

    def index(self, row, column, parent):
        if not self.hasIndex(row, column, parent):
            return QModelIndex()

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QModelIndex()

    def parent(self, index):
        if not index.isValid():
            return QModelIndex()

        childItem = index.internalPointer()
        parentItem = childItem.parent()

        if parentItem == self.rootItem:
            return QModelIndex()

        return self.createIndex(parentItem.row(), 0, parentItem)

    def rowCount(self, parent):
        if parent.column() > 0:
            return 0

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        return parentItem.childCount()

    # load list to viewer
    def setupModelData(self, data, parent, settings):
        print('Parsing Settings')
        selected_archives = []
        for child in settings.children():
            if isinstance(child, QCheckBox):
                if child.isChecked():
                    selected_archives.append(
                        archiveName_dict[child.text().split(' ')[0]])

        print(selected_archives)
        print('Setting up data')
        step = 0
        for i in selected_archives:
            step += len(data[i][3])
        step = float(step / 100)
        prog = 0
        count = 0
        for i in selected_archives:
            entry = data[i]
            arch_parent = TreeItem((entry[0], entry[1], entry[2]), parent)
            parent.appendChild(arch_parent)
            for kid in entry[3]:
                arch_parent.appendChild(
                    TreeItem((kid[0], int(kid[1]), int(kid[2]), kid[3]), arch_parent))
                if count > step:
                    prog += 1
                    self.progressTrigger.emit(prog)
                    QCoreApplication.sendPostedEvents()
                    count = 0
                else:
                    count += 1
        self.progressTrigger.emit(100)


class MyTableView(QTableView):

    def __init__(self, parent=None):
        QTableView.__init__(self, parent=None)

    def keyPressEvent(self, event):
        '''Use the default fallback'''
        QTableView.keyPressEvent(self, event)

    def editorDestroyed(self, editor):
        print(editor)
        self.dataChanged()
    # def commitData(self,editor):
    #    print('Commiting Data')
    #    print(dir(editor))
    #    print(editor.children[0])


class MyTableModel(QAbstractTableModel):

    def __init__(self, mylist, header, *args):
        QAbstractTableModel.__init__(self, parent=None, *args)
        self.mylist = mylist
        self.header = header

    def rowCount(self, parent=None):
        return len(self.mylist)

    def columnCount(self, parent=None):
        return len(self.mylist[0])

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role == Qt.EditRole:
            return self.mylist[index.row()][index.column()]
        elif role != Qt.DisplayRole:
            return None
        return self.mylist[index.row()][index.column()]

    def setData(self, index, value, role=Qt.EditRole):
        self.mylist[index.row()][index.column()] = str(value)
        return True

    def flags(self, index):
        if index.column() == 4:
            return Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsEditable
        else:
            return Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.header[col]
        return None

    def sort(self, col, order):
        """sort table by given column number col"""
        self.emit(SIGNAL("layoutAboutToBeChanged()"))
        self.mylist = sorted(self.mylist,
                             key=operator.itemgetter(col))
        if order == Qt.DescendingOrder:
            self.mylist.reverse()
        self.emit(SIGNAL("layoutChanged()"))

    def findlayer(self, name):
        """
        Find a layer in the model by it's name
        """
        for colId in range(self.columnCount()):
            startindex = self.index(0, colId)
            items = self.match(startindex, Qt.DisplayRole, name,
                               1, Qt.MatchExactly | Qt.MatchWrap | Qt.MatchContains)
            try:
                return items[0]
            except:
                continue
        return QModelIndex()


class SortModel(QSortFilterProxyModel):

    def __init__(self, parent=None):
        super(SortModel, self).__init__(parent)
        self.model = self.sourceModel()

    def lessThan(self, left, right):
        # print(left,right)
        leftData = self.sourceModel().data(left, self.sortRole())
        rightData = self.sourceModel().data(right, self.sortRole())

        try:
            return int(leftData) < int(rightData)
        except ValueError:
            return leftData < rightData

    def filterAcceptsRow(self, row_num, source_parent):
        ''' Overriding the parent function '''
        model = self.sourceModel()
        source_index = model.index(row_num, 0, source_parent)
        offset_index = model.index(row_num, 1, source_parent)

        if self.filterRegExp().pattern() in model.data(source_index, Qt.DisplayRole) or \
           self.filterRegExp().pattern() in str(model.data(offset_index, Qt.DisplayRole)):
            return True
        return False


# app = QApplication(sys.argv)
# form = IffEditorWindow()
# form.show()
# app.exec_()
