from PySide.QtCore import *
from PySide.QtGui import *

from myqmodels import MyTableModel
import sys
import os
import struct
from StringIO import StringIO

from despic_general import load_filedb, load_filedb_jpk


class MainWindow(QMainWindow):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setWindowTitle('Minion Rush File Explorer')
        self.resize(800, 600)

        # Main Layout
        tview = QTableView(parent=self)
        tview.horizontalHeader().setResizeMode(
            QHeaderView.Interactive)
        tview.contextMenuEvent = self.rowClick
        tview.horizontalHeader().setMovable(True)
        tview.setSortingEnabled(True)
        tview.sortByColumn(0, Qt.AscendingOrder)
        tview.setSelectionBehavior(QAbstractItemView.SelectRows)
        tview.setEditTriggers(QAbstractItemView.SelectedClicked)

        self.setCentralWidget(tview)

        # Add Menu
        menuBar = QMenuBar(parent=self)
        fileMenu = QMenu('File', parent=menuBar)
        fileMenu.addAction('Open')
        fileMenu.addAction('Exit')
        menuBar.addMenu(fileMenu)
        self.setMenuBar(menuBar)
        self.menuBar().triggered.connect(self.menuTrigger)

        # Add StatusBar
        self.setStatusBar(QStatusBar())
        # Active File Handles
        self.fileHandle = None

    def menuTrigger(self, val):
        if val.text() == 'Open':  # Opening File
            fileName, opts = QFileDialog.getOpenFileName(
                self, self.tr('Open Game Archive'), ".", self.tr('Bin Files(*.bin);;JPK DLC FIles(*.jpk)'))
            if fileName:
                self.openFile(fileName)  # Loading Bin File to Interface

        elif val.text() == 'Exit':
            print('Closing Application')
            self.fileHandle.close()
            sys.exit(0)

    def openFile(self, fileName):
        fileName = os.path.abspath(fileName)
        baseName = os.path.basename(fileName)
        dirName = os.path.dirname(fileName)

        ext = fileName.split('.')[-1]
        if ext == 'bin':
            # Trying to Locate the appropriate hdr File
            try:
                fileName = os.path.join(dirName, baseName)
                self.fileHandle = open(fileName, 'rb')
                baseName = baseName + '.hdr'
                fileName = os.path.join(dirName, baseName)

                # Loading Contents to Interface
                filedb = load_filedb(open(fileName, 'rb'))
                model = MyTableModel(
                    filedb, ['Name', 'Offset', 'Size1', 'Size2', 'Size3'])
                self.centralWidget().setModel(model)

                # Notifying User
                self.statusBar().showMessage('Opening File: ' + fileName)
            except OSError:
                self.statusBar().showMessage('Header File Missing...')
                return

        elif ext == 'jpk':
            # Loading Immediately jpk file
            self.fileHandle = open(fileName, 'rb')
            # Loading Contents to Interface
            filedb = load_filedb_jpk(open(fileName, 'rb'))
            model = MyTableModel(
                filedb, ['Name', 'Offset', 'Size'])
            self.centralWidget().setModel(model)

            # Notifying User
            self.statusBar().showMessage('Opening File: ' + fileName)

    def rowClick(self, event):
        menu = QMenu(self.centralWidget())
        exportaction = menu.addAction('Export Files')
        action = menu.exec_(self.centralWidget().mapToGlobal(event.pos()))
        if action == exportaction:
            print('Exporting Files')
            self.exportFiles()

    def exportFiles(self):
        selection = self.centralWidget().selectionModel()
        # print selection.selectedRows()
        dirName = QFileDialog.getExistingDirectory(parent=self, caption='Select Export Directory',
                                                   options=QFileDialog.ShowDirsOnly)
        for index in selection.selectedRows():
            row = index.row()
            entry = self.centralWidget().model().mylist[row]
            print entry[0]
            # print os.path.split(entry[0])
            fileName = list(os.path.split(entry[0]))[-1]

            fileOffset = entry[1]
            fileSize = entry[2]

            # Load File
            self.fileHandle.seek(fileOffset)
            t = StringIO()
            t.write(self.fileHandle.read(fileSize))
            t.seek(0)

            exportPath = os.path.join(os.path.abspath(dirName), fileName)

            f = open(exportPath, 'wb')
            f.write(t.read())
            f.close()
            self.statusBar().showMessage('File Saved at ' + exportPath)

app = QApplication(sys.argv)
form = MainWindow()
form.show()
app.exec_()
